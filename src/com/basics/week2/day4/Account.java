package com.basics.week2.day4;
import java.util.Scanner;

/**
 * Create a checked exception class InsufficientBalance .
 * 
 * Create a class Account with following members:
 * 1) accholdername
 * 2) balance
 * 3) parameterized constructor with opening balance as 1000 min and  if balance < 1000
 * then throw InsufficientBalance exception with a message, cannot open account with low balance
 * 4) create only getters for balance and name
 * 5) withdraw(double amount) that checks if balance > amount then let it withdraw and return true
 * else throw an exception InsufficientBalance exception with a message, cannot withdraw as balance is less
 * than withdrawal amount
 * 6) transfer(Account to, double amount) => check if enough balance in from account using withdraw function
 * and if balance available then withdraw  and deposit respectively
 */

public class Account {

	private String accholdername;
	private int balance;


	public Account(String accholdername,int balance) throws InsufficientBalance {
		super();

		if(balance < 1000)
		{
			throw new InsufficientBalance("Cannot open account with low balance");

		}
		else {
			this.balance = balance;
			this.accholdername=accholdername;
		}
	}


	public String getAccholdername() {
		return accholdername;
	}

	public int getBalance() {
		return balance;
	}
	public boolean withdraw(double amount) throws InsufficientBalance{
		if(balance>amount) {
			balance=(int)(balance-amount);
			return true;
		}
		else
		{
			throw new InsufficientBalance("Low balance, Withdrawal can't be done");
		}
	}
	public void transfer(Account to,double amount) throws InsufficientBalance
	{
		if(withdraw(amount)) {
			to.balance=(int)(to.balance+amount);
		}

	}
	public static void main(String[] args) {
		try {
			Account a1=new Account("dev",1000000);
			Account a2=new Account("mishra",50000000);
			a2.transfer(a1, 20000);
			System.out.println("account 1 balance:"+a1.getBalance());
			System.out.println("account 2 balance:"+a2.getBalance());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}