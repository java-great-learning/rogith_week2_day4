package com.basics.week2.day4;

public class ExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = new int[] {1,3,3,4,5};
		int a=arr[0];
		int b=arr[1];
		try {
		System.out.println(a/b);
		System.out.println(arr[1]);
		
		}catch(ArithmeticException e){
			System.out.println(e.getMessage());
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println(e);
		}
	}

}
